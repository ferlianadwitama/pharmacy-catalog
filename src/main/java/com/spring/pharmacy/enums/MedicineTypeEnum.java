package com.spring.pharmacy.enums;

public enum MedicineTypeEnum {
    BEBAS,
    KERAS,
    TERBATAS,
    NARKOTIK,
    FITOFARMAKA,
    HERBAL_TERSTANDAR,
    HERBAL
}
