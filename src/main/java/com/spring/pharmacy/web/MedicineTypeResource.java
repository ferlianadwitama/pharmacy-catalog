package com.spring.pharmacy.web;

import com.spring.pharmacy.dto.medicine_type.MedicineTypeCreateDTO;
import com.spring.pharmacy.dto.medicine_type.MedicineTypeResponseDTO;
import com.spring.pharmacy.service.MedicineTypeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@RestController
public class MedicineTypeResource {

    private final MedicineTypeService medicineTypeService;

    @GetMapping("/medicine/type/{code}")
    public ResponseEntity<MedicineTypeResponseDTO> findMedicineTypeByCode(@PathVariable String code) {
        MedicineTypeResponseDTO dto = medicineTypeService.findMedicineTypeByCode(code);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/medicine/type")
    public ResponseEntity<List<MedicineTypeResponseDTO>> findListMedicineType() {
        List<MedicineTypeResponseDTO> dtos = medicineTypeService.findAllMedicineType();

        return ResponseEntity.ok().body(dtos == null ? Collections.emptyList() : dtos);
    }

    @PostMapping("/medicine/type")
    public ResponseEntity<Void> createMedicineType(@RequestBody MedicineTypeCreateDTO dto) {
        medicineTypeService.createNewType(dto);
        return ResponseEntity.created(URI.create("/medicine/type")).build();
    }

}
