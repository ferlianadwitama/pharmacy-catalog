package com.spring.pharmacy.web;

import com.spring.pharmacy.dto.medicine_category.MedicineCategoryCreateDTO;
import com.spring.pharmacy.dto.medicine_category.MedicineCategoryResponseDTO;
import com.spring.pharmacy.service.MedicineCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@RestController
public class MedicineCategoryResource {

    private final MedicineCategoryService medicineCategoryService;

    @GetMapping("/medicine/category/{code}")
    public ResponseEntity<MedicineCategoryResponseDTO> findMedicineTypeByCode(@PathVariable String code) {
        MedicineCategoryResponseDTO dto = medicineCategoryService.findMedicineCategoryByCode(code);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping("/medicine/category")
    public ResponseEntity<List<MedicineCategoryResponseDTO>> findListMedicineType() {
        List<MedicineCategoryResponseDTO> dtos = medicineCategoryService.findAllCategory();

        return ResponseEntity.ok().body(dtos == null ? Collections.emptyList() : dtos);
    }

    @PostMapping("/medicine/category")
    public ResponseEntity<Void> createMedicineType(@RequestBody MedicineCategoryCreateDTO dto) {
        medicineCategoryService.createNewCategory(dto);
        return ResponseEntity.created(URI.create("/medicine/category")).build();
    }


}
