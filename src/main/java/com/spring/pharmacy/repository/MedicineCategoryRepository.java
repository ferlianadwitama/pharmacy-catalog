package com.spring.pharmacy.repository;

import com.spring.pharmacy.domain.MedicineCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineCategoryRepository extends JpaRepository<MedicineCategory, String> {
}
