package com.spring.pharmacy.repository;

import com.spring.pharmacy.domain.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineRepository extends JpaRepository<Medicine, Long> {
}
