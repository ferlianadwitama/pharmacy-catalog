package com.spring.pharmacy.repository;

import com.spring.pharmacy.domain.MedicineType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineTypeRepository extends JpaRepository<MedicineType, String> {
}
