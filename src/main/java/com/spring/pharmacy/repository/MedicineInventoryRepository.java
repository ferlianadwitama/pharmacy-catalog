package com.spring.pharmacy.repository;

import com.spring.pharmacy.domain.MedicineInventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineInventoryRepository extends JpaRepository<MedicineInventory, Long> {
}
