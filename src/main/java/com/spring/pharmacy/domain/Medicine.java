package com.spring.pharmacy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "medicine")
public class Medicine extends AbstractBaseEntity {

    private static final long serialVersionUID = -7810563460349213191L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "medicine_name", nullable = false)
    private String medicineName;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "dosage", nullable = false)
    private String dosage;

    @Column(name = "usage_instruction", nullable = false)
    private String usageInstruction;

    @ManyToOne
    @JoinColumn(name = "category_code", nullable = false)
    private MedicineCategory medicineCategory;

    @ManyToOne
    @JoinColumn(name = "type_code", nullable = false)
    private MedicineType medicineType;

//    @ManyToMany
//    @JoinTable(
//        name = "medicine_inventory",
//        joinColumns = { @JoinColumn(name = "medicine_id", referencedColumnName = "id") },
//        inverseJoinColumns = { @JoinColumn(name = "pharmacy_id", referencedColumnName = "id") }
//    )
//    private List<Pharmacy> pharmacies;
}
