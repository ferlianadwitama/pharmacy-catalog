package com.spring.pharmacy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "medicine_inventory")
public class MedicineInventory implements Serializable {

    private static final long serialVersionUID = 3503176174319803368L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quantity", nullable = false)
    private String quantity;

    @Column(name = "price", nullable = false)
    private int price;

    @Column(name = "expired_date", nullable = false)
    private String expiredDate;

}
