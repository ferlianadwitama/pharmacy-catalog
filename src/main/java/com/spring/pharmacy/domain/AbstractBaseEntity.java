package com.spring.pharmacy.domain;

import javax.persistence.Column;
import javax.persistence.Index;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@Table(indexes = { @Index(name = "uk_secure_id", columnList = "secure_id") })
public abstract class AbstractBaseEntity implements Serializable {

    private static final long serialVersionUID = -5940132380587760044L;

    @Column(name = "secure_id", unique = true)
    private String secureId = UUID.randomUUID().toString() ;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private Boolean deleted;

}
