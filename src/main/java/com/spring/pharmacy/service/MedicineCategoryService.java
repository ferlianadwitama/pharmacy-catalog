package com.spring.pharmacy.service;

import com.spring.pharmacy.dto.medicine_category.MedicineCategoryCreateDTO;
import com.spring.pharmacy.dto.medicine_category.MedicineCategoryResponseDTO;

import java.util.List;

public interface MedicineCategoryService {

    List<MedicineCategoryResponseDTO> findAllCategory();
    MedicineCategoryResponseDTO findMedicineCategoryByCode(String code);
    void createNewCategory(MedicineCategoryCreateDTO dto);

}
