package com.spring.pharmacy.service.impl;

import com.spring.pharmacy.service.PharmacyService;
import org.springframework.stereotype.Service;

@Service("pharmacyService")
public class PharmacyServiceImpl implements PharmacyService {
}
