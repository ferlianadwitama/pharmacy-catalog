package com.spring.pharmacy.service.impl;

import com.spring.pharmacy.domain.MedicineType;
import com.spring.pharmacy.dto.medicine_type.MedicineTypeCreateDTO;
import com.spring.pharmacy.dto.medicine_type.MedicineTypeResponseDTO;
import com.spring.pharmacy.exception.BadRequestException;
import com.spring.pharmacy.repository.MedicineRepository;
import com.spring.pharmacy.repository.MedicineTypeRepository;
import com.spring.pharmacy.service.MedicineTypeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("medicineTypeService")
@AllArgsConstructor
public class MedicineTypeServiceImpl implements MedicineTypeService {

    private final MedicineTypeRepository medicineTypeRepository;

    @Override
    public List<MedicineTypeResponseDTO> findAllMedicineType() {
        List<MedicineType> medicineType = medicineTypeRepository.findAll();
        List<MedicineTypeResponseDTO> list = medicineType.stream().map((type) -> {
            MedicineTypeResponseDTO dto =
                    new MedicineTypeResponseDTO(
                        type.getTypeCode(),
                        type.getTypeName(),
                        type.getDescription()
                    );

            return dto;
        }).collect(Collectors.toList());

        return list;
    }

    @Override
    public MedicineTypeResponseDTO findMedicineTypeByCode(String code) {
        MedicineType type =
                medicineTypeRepository
                .findById(code)
                .orElseThrow(() -> new BadRequestException("MedicineType.not.found"));

        MedicineTypeResponseDTO dto =
                new MedicineTypeResponseDTO(
                    type.getTypeCode(),
                    type.getTypeName(),
                    type.getDescription()
                );
        return dto;
    }

    @Override
    public void createNewType(MedicineTypeCreateDTO dto) {
        MedicineType type = new MedicineType(
                dto.getTypeCode(),
                dto.getTypeName(),
                dto.getDescription()
        );

        medicineTypeRepository.save(type);
    }
}
