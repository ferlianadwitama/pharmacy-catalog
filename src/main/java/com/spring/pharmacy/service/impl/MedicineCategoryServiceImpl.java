package com.spring.pharmacy.service.impl;

import com.spring.pharmacy.domain.MedicineCategory;
import com.spring.pharmacy.dto.medicine_category.MedicineCategoryCreateDTO;
import com.spring.pharmacy.dto.medicine_category.MedicineCategoryResponseDTO;
import com.spring.pharmacy.exception.BadRequestException;
import com.spring.pharmacy.repository.MedicineCategoryRepository;
import com.spring.pharmacy.service.MedicineCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service("medicineCategoryService")
public class MedicineCategoryServiceImpl implements MedicineCategoryService {

    private final MedicineCategoryRepository medicineCategoryRepository;

    @Override
    public List<MedicineCategoryResponseDTO> findAllCategory() {
        List<MedicineCategory> medicineType = medicineCategoryRepository.findAll();
        List<MedicineCategoryResponseDTO> list = medicineType.stream().map((type) -> {
            MedicineCategoryResponseDTO dto =
                    new MedicineCategoryResponseDTO(
                            type.getCategoryCode(),
                            type.getCategoryName()
                    );

            return dto;
        }).collect(Collectors.toList());

        return list;
    }

    @Override
    public MedicineCategoryResponseDTO findMedicineCategoryByCode(String code) {
        MedicineCategory type =
                medicineCategoryRepository
                        .findById(code)
                        .orElseThrow(() -> new BadRequestException("MedicineCategory.not.found"));

        MedicineCategoryResponseDTO dto =
                new MedicineCategoryResponseDTO(
                        type.getCategoryCode(),
                        type.getCategoryName()
                );
        return dto;
    }

    @Override
    public void createNewCategory(MedicineCategoryCreateDTO dto) {
        MedicineCategory type = new MedicineCategory(
                dto.getCategoryCode(),
                dto.getCategoryName()
        );

        medicineCategoryRepository.save(type);
    }
}
