package com.spring.pharmacy.service.impl;

import com.spring.pharmacy.service.MedicineInventoryService;
import org.springframework.stereotype.Service;

@Service("medicineInventoryService")
public class MedicineInventoryServiceImpl implements MedicineInventoryService {
}
