package com.spring.pharmacy.service.impl;

import com.spring.pharmacy.service.MedicineService;
import org.springframework.stereotype.Service;

@Service("medicineService")
public class MedicineServiceImpl implements MedicineService {
}
