package com.spring.pharmacy.service;

import com.spring.pharmacy.dto.medicine_type.MedicineTypeCreateDTO;
import com.spring.pharmacy.dto.medicine_type.MedicineTypeResponseDTO;

import java.util.List;

public interface MedicineTypeService {

    List<MedicineTypeResponseDTO> findAllMedicineType();
    MedicineTypeResponseDTO findMedicineTypeByCode(String code);
    void createNewType(MedicineTypeCreateDTO dto);

}
